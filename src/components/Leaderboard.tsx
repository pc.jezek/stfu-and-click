import {Team} from "../models/Team";
import React, {useEffect} from "react";
import styled from 'styled-components'

const LeaderboardRow = styled.div`
  padding: 0 3vw 0 3vw;
  display: flex;
  flex-direction: row;
  align-content: flex-start;
  font-weight: bold;
  
  span.order {
    flex-basis: 6vw;
    flex-shrink: 0;
  }
  
  span.name {
    flex-grow:1;
  }
  
  span.clicks {
    flex-basis: 10vw;
    text-align: right;
    flex-shrink: 0; 
  }
`;

const LeaderboardItem = styled(LeaderboardRow)`
  padding: ${props => props.highlighted ? '.7vh 2.5vw .7vh 2.5vw' : '1vh 3vw 1vh 3vw'};
  font-size: ${props => props.highlighted ? '2em' : 'normal'};
  color: ${props => props.highlighted ? 'white' : 'black'};
  background: ${props => props.highlighted ? '#4990E2' : (props.odd ? '#DCE9F9' : '#ECF3FD')};
`;

interface LeaderboardProps {
  items: Team[],
  windowSize?: number,
  highlight: string,
  onReload: () => void,
}

export function Leaderboard(props: LeaderboardProps) {
  const {items, highlight, onReload} = props;
  useEffect(() => {
    onReload();
    const timeout = setInterval(onReload, 3000);
    return () => clearInterval(timeout);
  }, [onReload]);
  return (
    <div>
      <LeaderboardRow>
        <span className='order'>#</span>
        <span className='name'>TEAM</span>
        <span className='clicks'>CLICKS</span>
      </LeaderboardRow>
      {items.map((i, idx) =>
        <LeaderboardItem key={i.teamName}
                         odd={i.order % 2 === 1}
                         highlighted={highlight === i.teamName}>
          <span className='order'>{i.order}</span>
          <span className='name'>{i.teamName}</span>
          <span className='clicks'>{i.teamClicks}</span>
        </LeaderboardItem>)}
    </div>
  );
}
