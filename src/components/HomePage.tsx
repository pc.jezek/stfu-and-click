import React, {useState} from "react";
import {Link} from 'react-router-dom';
import {LeaderboardContainer} from "../containers/LeaderboardContainer";
import {Ribbon} from "../common/Ribbon";
import {BorderBox} from "../common/BorderBox";
import styled from 'styled-components';
import {Page} from "../common/Page";
import {ClickButton} from "../common/ClickButton";

const BoxTop = styled.div`
  padding: 1vh 1vw 1vh 1vw;
  display: flex;
  justify-content: center;
  align-items: center;
  
  .input-header-text{
    font-style: italic;
    margin-bottom: 8px;
  };
  
  .input-box {
    padding-right: 1vw;
    input {
      font-size: 16pt;
      border: 1px solid grey;
      padding: 4px;
      border-radius: 4px;
    }
  }
`;

const StyledLink = styled(Link)`
  text-decoration: none;
  width: 300px;
  height: 70px;
`;

const AnonymousQuote = styled.div`
  padding: 3vh 0 3vh 0;
  font-style: italic;
  text-align: center;
  
  div.quote-author{
    position: relative;
    left: 160px;
  }
    
`;

export const HomePage = () => {
  const [value, setValue] = useState('');
  return (
    <Page>
      <AnonymousQuote>
        <div className='quote-text'>"It's really simple, you just need to click as fast as you can."</div>
        <div className='quote-author'>-anonymous</div>
      </AnonymousQuote>
      <BorderBox>
        <BoxTop>
          <div className='input-box'>
            <div className='input-header-text'>Enter your team name:</div>
            <input type='text' placeholder='Your mom'
                   onChange={(v) => setValue(v.target.value)}/>
          </div>
          <StyledLink to={`/${value}`}>
            <ClickButton>CLICK!</ClickButton>
          </StyledLink>
        </BoxTop>
        <Ribbon>TOP 10 Clickers</Ribbon>
        <LeaderboardContainer windowSize={10}/>
      </BorderBox>
    </Page>
  );
};

