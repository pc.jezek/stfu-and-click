import React from "react";
import {LeaderboardContainer} from "../containers/LeaderboardContainer";
import {BorderBox} from "../common/BorderBox";
import styled from 'styled-components';
import {Page} from "../common/Page";
import {ClickButton} from "../common/ClickButton";

const StyledHeader = styled.h1`
  text-align: center;
  font-weight: normal;
  span.team-name {
    font-weight: bold;
  }
`;

const URLText = styled.div`
  font-style: italic;
  padding: 3vh 0 3vh 0;
  font-style: italic;
  text-align: center;
  input {
    wrap: soft;
    resize: none;
    background: white;
    border: 1px solid grey;
    border-radius: 4px;
    padding: 4px;
  }
`;

const ClickStats = styled.div`
  padding: 2vh 0 2vh 0;
  display: flex;
  justify-content: space-around;
  
  .clickNumber{
    text-align: center;
    font-size: 36pt;
    color: #488FE2;
    font-weight: bold;
  }
`;

const StyledClickButton = styled.div`
  padding: 0 1vh 0 1vh;
  height: 80px;
`;

export const TeamPage = ({selectedTeam, teamClicks, onClick}) => {
  const location = window.location.href
  const windowSize = +(process.env.REACT_APP_LEADERBOARD_WINDOW_SIZE || 9);
  return (<Page>
    <StyledHeader>Clicking for team <span className='team-name'>{selectedTeam.teamName}</span></StyledHeader>
    <URLText>
      Too lazy to click? Let your pals click for you: <input readOnly size={location.length} value={location}/>
    </URLText>
    <BorderBox>
      <StyledClickButton onClick={onClick}>
        <ClickButton>CLICK!</ClickButton>
      </StyledClickButton>
      <ClickStats>
        <div>Your clicks:
          <div className='clickNumber'>{selectedTeam.yourClicks}</div>
        </div>
        <div>Team clicks:
          <div className='clickNumber'>{teamClicks}</div>
        </div>
      </ClickStats>
      <LeaderboardContainer windowSize={windowSize}
                            highlight={selectedTeam.teamName}/>
    </BorderBox>
  </Page>)
};
