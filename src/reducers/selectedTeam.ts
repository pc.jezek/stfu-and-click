import {
  CLICK_TEAM_FAILURE, CLICK_TEAM_REQUEST,
  CLICK_TEAM_SUCCESS, TEAM_RESET,
  TEAM_SELECTED
} from "../actions/actions";
import {SelectedTeam} from "../models/Team";

const initialState: SelectedTeam = {
  teamName: undefined,
  session: '',
  isFetching: false,
  yourClicks: 0
};

export const selectedTeam = (state: SelectedTeam = initialState, action: any) => {
  switch (action.type) {
    case(TEAM_SELECTED):
      return Object.assign({}, state, {
        teamName: action.selectedTeam.teamName,
        session: action.selectedTeam.session,
        yourClicks: 0
      });
    case(TEAM_RESET):
      return Object.assign({}, initialState);
    case(CLICK_TEAM_SUCCESS):
      return Object.assign({}, state, {
        isFetching: false,
        yourClicks: action.response.your_clicks
      });
    case(CLICK_TEAM_FAILURE):
      return Object.assign({}, state, {isFetching: false});
    case(CLICK_TEAM_REQUEST):
      return Object.assign({}, state, {isFetching: true});
    default:
      return state
  }
};
