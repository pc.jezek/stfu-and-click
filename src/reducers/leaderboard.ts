import {Team} from "../models/Team";
import {CLICK_TEAM_SUCCESS, LOAD_TEAMS_FAILURE, LOAD_TEAMS_REQUEST, LOAD_TEAMS_SUCCESS} from "../actions/actions";

export const leaderboard = (state: { isFetching: boolean, items: Team[] } = {
  isFetching: false,
  items: []
}, action: any) => {

  switch (action.type) {
    case(LOAD_TEAMS_SUCCESS):
      return Object.assign({}, state, {isFetching: false, items: action.teams});
    case(LOAD_TEAMS_FAILURE):
      return Object.assign({}, state, {isFetching: false,});
    case(LOAD_TEAMS_REQUEST):
      return Object.assign({}, state, {isFetching: true,});
    case(CLICK_TEAM_SUCCESS):
      // Update team clicks for clicked team
      return Object.assign({}, state, {
        items: state.items.map(i => i.teamName === action.teamName ?
          {...i, teamClicks: action.response.team_clicks} :
          i)
      });
    default:
      return state
  }
};
