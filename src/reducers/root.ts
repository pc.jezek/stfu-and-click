import {combineReducers} from "redux";
import {leaderboard} from "./leaderboard";
import {selectedTeam} from "./selectedTeam";

export const rootReducer = combineReducers({
    leaderboard,
    selectedTeam
  }
);
