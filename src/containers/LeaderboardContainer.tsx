import {connect} from "react-redux";
import {fetchTeams} from "../actions/actions";
import {Leaderboard} from "../components/Leaderboard";

const mapStateToProps = (state: any, ownProps: any) => {
  const items = state.leaderboard.items;
  const highlightIdx = items.findIndex(t => t.teamName === state.selectedTeam.teamName);
  let windowStart = highlightIdx === -1 ?
    items.length - ownProps.windowSize :
    Math.max(0, highlightIdx - (ownProps.windowSize - 1) / 2);
  console.log(windowStart)

  let windowEnd = windowStart + ownProps.windowSize;
  console.log(windowStart, windowEnd)

  if (windowEnd > items.length) {
    windowStart = Math.max(0, items.length - ownProps.windowSize);
  }
  windowEnd = Math.min(items.length, windowEnd);

  const itemsFiltered = state.selectedTeam.teamName ?
    items.slice(windowStart, windowEnd) :
    items.slice(0, ownProps.windowSize);
  return {
    items: itemsFiltered,
    highlighted: state.selectedTeam.teamName,
  }
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onReload: () => {
      dispatch(fetchTeams())
    }
  }
};

export const LeaderboardContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Leaderboard);
