import {clickTeam, teamReset, teamSelected} from "../actions/actions";
import {connect} from "react-redux";
import React, {useEffect} from "react";
import {TeamPage} from "../components/TeamPage";

interface TeamPageWrapperProps {
  selectedTeam: { teamName: string; session: string }
  teamClicks: number,
  onClick: (teamName: string, session: string) => void;
  onTeamChange: (selectedTeamUrl: string) => void;
  onTeamReset: () => void;
  selectedTeamURL: string;
}

const TeamPageWrapper = (props: TeamPageWrapperProps) => {
  const {selectedTeam, teamClicks, onClick, onTeamChange, selectedTeamURL, onTeamReset} = props;
  useEffect(() => {
    onTeamChange(selectedTeamURL);
    return onTeamReset;
  }, [onTeamReset, onTeamChange, selectedTeamURL]);
  return (<TeamPage selectedTeam={selectedTeam}
                    teamClicks={teamClicks}
                    onClick={() => onClick(selectedTeam.teamName, selectedTeam.session)}/>);
};

const mapStateToProps = (state: any, ownProps: any) => {
  const selectedTeam = state.selectedTeam;
  const team = state.leaderboard.items.find(t => t.teamName === selectedTeam.teamName);
  const teamClicks = team ? team.teamClicks : 0;
  return {
    selectedTeamURL: ownProps.match.params.selected,
    selectedTeam,
    teamClicks,
  }
};

const mapDispatchToProps = (dispatch: any) => ({
  onTeamChange: (teamName) => {
    dispatch(teamSelected(teamName))
  },
  onClick: (teamName, session) => {
    dispatch(clickTeam(teamName, session))
  },
  onTeamReset: () => {
    dispatch(teamReset());
  }
});


export const TeamPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(TeamPageWrapper);


