import {Team, TeamClickRequest, TeamClickResponse, TeamResponse} from "../models/Team";

export const TEAM_SELECTED = 'TEAM_SELECTED';
export const TEAM_RESET = 'TEAM_RESET';
export const CLICK_TEAM_REQUEST = 'CLICK_TEAM_REQUEST';
export const CLICK_TEAM_SUCCESS = 'CLICK_TEAM_SUCCESS';
export const CLICK_TEAM_FAILURE = 'CLICK_TEAM_FAILURE';
export const LOAD_TEAMS_REQUEST = 'LOAD_TEAMS_REQUEST';
export const LOAD_TEAMS_SUCCESS = 'LOAD_TEAMS_SUCCESS';
export const LOAD_TEAMS_FAILURE = 'LOAD_TEAMS_FAILURE';

export const API_URL = process.env.REACT_APP_API_URL;

export const loadTeamsRequest = () => ({
  type: LOAD_TEAMS_REQUEST,
});

export const loadTeamsSuccess = (teams: TeamResponse[]) => ({
  type: LOAD_TEAMS_SUCCESS,
  teams: teams.map(t => new Team(t))
});

export const loadTeamsFailure = () => ({
  type: LOAD_TEAMS_FAILURE,
});

export const fetchTeams = () => (dispatch: any) => {
  dispatch(loadTeamsRequest());
  return fetch(`${API_URL}/leaderboard`)
    .then(
      response => response.json() as Promise<TeamResponse[]>,
      error => dispatch(loadTeamsFailure()))
    .then(teams => dispatch(loadTeamsSuccess(teams)))
};

export const teamSelected = (teamName: string) => ({
  type: TEAM_SELECTED,
  selectedTeam: {
    teamName,
    session: Math.random().toString(25)
  }
});

export const teamReset = () => ({type: TEAM_RESET});

export const clickTeamRequest = (teamName: string) => ({
  type: CLICK_TEAM_REQUEST,
  teamName
});

export const clickTeamSuccess = (teamName: string, response: TeamClickResponse) => ({
  type: CLICK_TEAM_SUCCESS,
  teamName: teamName,
  response
});

export const clickTeamFailure = (teamName: string) => ({
  type: CLICK_TEAM_FAILURE,
});

export const clickTeam = (teamName: string, session) => (dispatch: any) => {
  dispatch(clickTeamRequest(teamName));
  const requestBody: TeamClickRequest = {team: teamName, session: session};
  return fetch(`${API_URL}/klik`, {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(requestBody)
  }).then(
    response => response.json() as Promise<TeamClickResponse>,
    error => dispatch(clickTeamFailure(teamName)))
    .then(
      teamClickResponse => {
        console.log(teamClickResponse);
        dispatch(clickTeamSuccess(teamName, teamClickResponse));
        dispatch(fetchTeams());
      })
};
