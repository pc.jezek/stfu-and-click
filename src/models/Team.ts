// API Response Items
export interface TeamResponse {
  order: number;
  team: string;
  clicks: number;
}

export interface TeamClickResponse {
  your_clicks: number,
  team_clicks: number,
}

export interface TeamClickRequest {
  team: string;
  session: string;
}

// Models
export class Team {
  order: number;
  teamName: string;
  teamClicks: number;

  constructor(teamFromAPI: TeamResponse) {
    this.order = teamFromAPI.order;
    this.teamName = teamFromAPI.team;
    this.teamClicks = teamFromAPI.clicks;
  }
}

export interface SelectedTeam {
  teamName: string | undefined,
  yourClicks: number,
  isFetching: boolean,
  session: string,
}

