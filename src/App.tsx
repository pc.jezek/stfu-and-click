import React from 'react';
import './App.css';
import {applyMiddleware, createStore} from "redux";
import {rootReducer} from "./reducers/root";
import {BrowserRouter as Router, Route} from 'react-router-dom'
import {Provider} from 'react-redux'
import thunk from "redux-thunk";
import {HomePage} from "./components/HomePage";
import {TeamPageContainer} from "./containers/TeamPageContainer";
import {TitleBar} from "./common/TitleBar";

//From https://redux.js.org/advanced/middleware
const logger = store => next => action => {
  console.log('dispatching', action);
  let result = next(action);
  console.log('next state', store.getState());
  return result;
};

const store = createStore(rootReducer, applyMiddleware(thunk, logger));

const App = () => {
  return (
    <Provider store={store}>
      <Router basename={process.env.PUBLIC_URL}>
        <Route path='/' component={TitleBar}/>
        <Route exact path='/' component={HomePage}/>
        <Route path='/:selected' component={TeamPageContainer}/>
      </Router>
    </Provider>
  );
};

export default App;
