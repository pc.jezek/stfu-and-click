import styled from 'styled-components';
import React from "react";

// Taken from https://freefrontend.com/css-ribbons/ (Tim D)
const StyledRibbon = styled.div`
  width: 250px;
  height: 50px;
  margin: 20px auto 40px;
  position: relative;
  color: #fff;
  font: 24px/50px sans-serif;
  font-weight: bold;
  text-align: center;
  background: #3D79D0;
  
  i {
    position: absolute;
    box-sizing: content-box;
  };
  
  i:first-child, i:nth-child(2) {
    position: absolute;
    left: -20px;
    bottom: -20px;
    z-index: -1;
    border: 20px solid transparent;
    border-right-color: #043140;
  };
  
  i:nth-child(2) {
    left: auto;
    right: -20px;
    border-right-color: transparent;
    border-left-color: #043140;
  };
  
  i:nth-child(3), i:last-child {
    width: 20px;
    bottom: -20px;
    left: -60px;
    z-index: -2;
    border: 30px solid #1D53C0;
    border-left-color: transparent;
  };

  i:last-child {
    bottom: -20px;
    left: auto;
    right: -60px;
    border: 30px solid #1D53C0;
    border-right-color: transparent;
  }
`;

export const Ribbon = (props) => (<StyledRibbon>{props.children}<i/><i/><i/><i/></StyledRibbon>);

