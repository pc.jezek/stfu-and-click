import styled from 'styled-components';
import {Link} from 'react-router-dom';
import React from "react";

const StyledTitleBar = styled.div`
  background-color: #488FE1;
  min-height: 2vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  font-weight: bold;
  color: white;
`;

const StyledLink = styled(Link)`text-decoration: none`;

export const TitleBar = () =>
  <StyledLink to='/'>
    <StyledTitleBar>{window.location.href}</StyledTitleBar>
  </StyledLink>;
