import styled from 'styled-components'
import React from "react";

const StyledPageBox = styled.div`
  padding-top: 1.5vh;
  border: 4px solid #518EDF;
  border-radius: 15px;
`;

const MotivationDiv = styled.div`
  padding: 1.5vh 1.5vw 1.5vh 1.5vw;
  font-style: italic;
  width: 100%;
  text-align: center;
`;

export const BorderBox = (props) =>
  (<StyledPageBox>{props.children}
    <MotivationDiv>Want to be top? STFU and click!</MotivationDiv>
  </StyledPageBox>);

