import styled from 'styled-components';
import React from "react";

const StyledButton = styled.div`
  cursor: pointer;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: #3D79D0;
  border: 8px solid #3D79D0;
  text-align: center;
  font-weight: bold;
  color: white;
  text-decoration: none;
  font-size: 28pt;
  border-radius: 12px;
  
  &:hover {
    box-shadow: 2px 2px 4px 0 rgba(0,0,0,0.5);
    background-color: #488FE1;
    border-color: #488FE1;
  }
`;

interface ClickButtonProps {
  onClick?: () => void,
  children: any;
}

export const ClickButton = (props: ClickButtonProps) =>
  <StyledButton onClick={props.onClick}>
    {props.children}
  </StyledButton>;
