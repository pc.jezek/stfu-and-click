import styled from 'styled-components';
import React from "react";

const StyledPage = styled.div`
  padding-top: 1vh;
  
  @media (min-width: 1024px) {
    width: 60%;
    margin-left: auto;
    margin-right: auto;
  }
`;

export const Page = (props) => <StyledPage>{props.children}</StyledPage>;
