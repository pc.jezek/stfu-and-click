#Created based on tutorial:
#https://mherman.org/blog/dockerizing-a-react-app/

FROM node:16-stretch as build

WORKDIR /usr/app

ENV PATH /usr/app/node_modules/.bin:$PATH

COPY . .

RUN yarn global add serve
RUN yarn install
RUN yarn build

FROM nginx:stable-alpine
COPY --from=build /usr/app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
